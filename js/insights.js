$(document).ready(function () {

    if ($(window).width() < 992) {

        // Preloader
        let tl = gsap.timeline({repeat: 0, repeatDelay: 0});
        tl.to($('.insights .description'), {delay: .5, duration: 1.3, opacity: '1'});

        $('.tap-btn ').on('click', function (){
            $(this).fadeOut();
            gsap.to($('.tap-btn '), {duration: 1.3, opacity: '0',onComplete: function () {
                    $('.tap-btn ').remove();
            }
            });
            tl.to($('.insights .description'), {duration: 1.3, opacity: '0'});
            tl.to($('.tap-btn '), {duration: 1.3, opacity: '0',onComplete: function () {
                $('.insights .left-column-wrapper').remove();
                $('.insights .right-column').show();
                tl.from($('.insights .right-column'), {delay: .5 , duration: 1.3, opacity: '0'});
            }
            },'<');
        })


    } else {
        let defaultImg = $('#image-insight').attr('src');

        $('.insights-item').mouseenter(function () {
            $('.insights-item .title').css('opacity', '0.5');
            $('.insights-item .descr').css('opacity', '0.2');
            $(this).find('.title').css('opacity', '1');
            $(this).find('.descr').css('opacity', '0.6');

            let src = $(this).attr('data-img-src');

            $('#image-insight').attr('src', src);
        });

        $('.insights-item').mouseleave(function () {
            $('.insights-item .title').css('opacity', '1');
            $('.insights-item .descr').css('opacity', '0.6');
            $('#image-insight').attr('src', defaultImg);
        });

    }


    $('.insights-image-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'ease-in-out',
        arrows: true,
        prevArrow: "<button type='button' class='slick-btn slick-prev'></button>",
        nextArrow: "<button type='button' class='slick-btn slick-next'></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows: false,
                    fade: false
                }
            }]
    });
});

