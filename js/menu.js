$(document).ready(function () {

    if ($(window).width() < 992) {
        $('.menu-btn').on('click', function () {
            // $('.main-icons').css('display', 'flex');
            $(this).hide();
            $('#main-menu').show();

            // $(document).mouseup(function (e) {
            //     var container = $(".main-icons");
            //     if (container.has(e.target).length === 0) {
            //         container.hide();
            //
            //         $('.mob-menu-btn').show();
            //     }
            // });
        });

        $('#trend-tab').on('click', function () {
            $(this).addClass('active');
            $('#credits-tab').removeClass('active');
            $('.menu-left-column').addClass('tab-active');
            $('.menu-right-column').removeClass('tab-active');
        });
        $('#credits-tab').on('click', function () {
            $(this).addClass('active');
            $('#trend-tab').removeClass('active');
            $('.menu-right-column').addClass('tab-active');
            $('.menu-left-column').removeClass('tab-active');
        });
        //
        // $('.menu-btn').on('click', function () {
        //     $('#main-menu').show();
        //     $('.main-icons').hide();
        //     $('.mob-menu-btn').show();
        // });

        $('.close-menu-btn').on('click', function () {
            $('#main-menu').hide();
            $('.menu-btn').show();
        });
    } else {
        // Menu show/hide

        $('.menu-btn').on('click', function () {
            $('#main-menu').show();
        });

        $('.close-menu-btn').on('click', function () {
            $('#main-menu').hide();
        });
    }

    // --------------Main menu scripts----------------

    // Change photo on hover trend title

    // $('.menu-trends-list').on('mouseover', '.menu-trends-item:not(.active)', function () {
    //     $(this).closest('.menu-trends-wrap').find('.img-item').hide().eq($(this).index()).fadeIn('fast');
    // });
    //
    // $('.menu-trends-item').on('mouseout', function () {
    //     $('.menu-trends-wrap .img-item').fadeOut();
    // });


});

