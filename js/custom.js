$(document).ready(function () {
    //---------------Explore button position centered------------
    function exploreBtnPos() {

        let heightWindow = $(window).height(),
            heightText = $('.second-text').height(),
            btnHeight = $('.explore-btn-wrapper').height() / 2,
            bottomPos = (heightWindow - heightText) / 4 - btnHeight + 25 + 'px';

        $('.explore-btn-wrapper').css('bottom', bottomPos);
    }

    exploreBtnPos();

    $(window).resize(exploreBtnPos);


    // ----------------------------Preloader----------------
    var tl = gsap.timeline({repeat: 0, repeatDelay: 0}),
        firstText = $('.first-text'),
        secondText = $('.second-text'),
        thirdText = $('.third-text'),
        fourthTitle = $('.fourth-title'),
        exploreBtn = $('.explore-btn-wrapper'),
        trendsImg = $('.trends-item .wrapper-item'),
        trendsItemText = $('.trends-item .text'),
        trendsList = $('.trends-list'),
        preloaderText = $('.preloader-text'),
        mainTitle = $('.main-tilte'),
        screenSwitchBtn = $('#second-format-btn'),
        iconsBlock = $('.main-icons');

    gsap.to(preloaderText, {delay: .1, duration: .3, opacity: '1', ease: Sine.easeInOut,})

    //--------------Mobile script --------------------------
    if ($(window).width() < 992) {
        // first screen
        gsap.from(firstText, {delay: 1, duration: 1.3, opacity: '0', ease: Sine.easeInOut,});

        // second screen
        $('.skip-btn1').on('click', function () {
            $(this).fadeOut();
            $('.skip-btn2').fadeIn();

            tl.to(firstText, {duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(firstText, 0, {
                display: "none", onComplete: function () {
                    firstText.remove();
                    $('.skip-btn1').remove();
                }
            });
            tl.to(secondText, 0, {display: "block"});
            tl.to($('.second-text .item-first'), {
                duration: 1.3, opacity: '1', ease: Sine.easeInOut,
            });

            $('.skip-btn2').on('click', function () {
                tl.to($('.second-text .item-first'), {
                    duration: 1.3,
                    opacity: '0',
                    ease: Sine.easeInOut
                });
                tl.to($('.skip-btn2'), {
                    duration: 1.3,
                    opacity: '0',
                    ease: Sine.easeInOut,
                    onComplete: function () {
                        $('.skip-btn2').remove();
                    }
                }, '<');

                tl.to($('.second-text .item-first'), 0, {
                    display: "none"
                });

                tl.to($('.second-text .item-second'), 0, {display: "block",}, '<');
                tl.to($('.second-text .item-second'), {duration: 1.3, opacity: '1', ease: Sine.easeInOut});
                tl.to(exploreBtn, 0, {display: "block"});
                tl.from(exploreBtn, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            });
        })

        //-------Slider trend init
        $('.trends-list').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            speed: 500,
            arrows: false,
            centerMode: true,
        });

        let firsTitleTrend = $('.slick-current').attr('data-title');
        $('.trend-title-mob').html(firsTitleTrend);


        // let OurString = $('.trend-title-mob').html(),
        //     NewOurString = "";
        //
        // OurString = OurString.split(" ");
        // for (let i = 0; i < OurString.length; i++) {
        //     NewOurString = NewOurString + "<div class='word-overflow'><span>" + OurString[i] + "</span></div>";
        // }
        // $('.trend-title-mob').html(NewOurString);


        //third screen and main screen
        exploreBtn.on('click', function () {
            gsap.to($('.menu-btn'), {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(secondText, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(exploreBtn, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut}, "<");
            tl.to(secondText, 0, {
                display: "none", onComplete: function () {
                    secondText.remove();
                }
            });
            tl.to(exploreBtn, 0, {
                display: "none", onComplete: function () {
                    exploreBtn.remove();
                }
            });
            // third screen
            tl.to(thirdText, 0, {display: "block"});
            tl.from(thirdText, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(thirdText, {delay: 1.5, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(thirdText, 0, {
                display: "none", onComplete: function () {
                    thirdText.remove();
                }
            }, ">-0.2");
            // fourth and main screen
            tl.to(fourthTitle, {display: "block"});
            tl.from(fourthTitle, {duration: 1.3, opacity: '0', ease: Sine.easeInOut}, ">-0.3");
            tl.to(trendsList, {
                display: "block", onComplete: function () {
                    $('.trends-list').slick('reinit');

                    $('.trends-list').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                        $('.trend-title-mob').fadeOut();
                    });

                    $('.trends-list').on('afterChange', function (event, slick, currentSlide, nextSlide) {
                        let currentTitle = $('.slick-current').attr('data-title');
                        // let NewOurString = "";
                        // currentTitle = currentTitle.split(" ");
                        // for (let i = 0; i < currentTitle.length; i++) {
                        //     NewOurString = NewOurString + "<div class='word-overflow'><span class='mob-word'>" + currentTitle[i] + "</span></div>";
                        // }
                        $('.trend-title-mob').html(currentTitle);
                        $('.trend-title-mob').fadeIn();


                        // TweenMax.staggerFrom($('.trend-title-mob .mob-word'), 0.3, {y: 100, stagger: 0.03});
                    });
                }
            });
            tl.from(trendsList, {onComplete: function () {
                    $('.trends-list').slick('reinit');

                }
            });
            tl.from(trendsList, {duration: 1, opacity: "0"}, '<');
            // tl.from(trendsImg, {duration: 2.5, width: "4px"}, "<");
            tl.from(trendsItemText, {duration: 1, opacity: "0"}, "<");
            tl.to(fourthTitle, {
                delay: 1.5, duration: 1.3, opacity: "0", onComplete: function () {
                    $('.logo').addClass('logo-mobile');
                }
            }, "<");
            tl.to(preloaderText, 0, {
                display: "none", onComplete: function () {
                    preloaderText.remove();

                }
            });
            tl.to(mainTitle, 0, {
                display: "block", onComplete: function () {
                    gsap.from(mainTitle, {duration: 1.3, opacity: '0', ease: Sine.easeInOut});
                    gsap.to($('.trend-title-mob'), 0, {display: 'block'});
                    gsap.from($('.trend-title-mob'), {duration: 1.3, opacity: '0', ease: Sine.easeInOut});
                    gsap.to($('.menu-btn'), {delay: 0, duration: 1.3, opacity: '1', ease: Sine.easeInOut});
                }
            }, "<-1");


        });
    }

    //--------------Desktop script --------------------------
    else {

        // first screen
        tl.from(firstText, {delay: 1, duration: 1.3, opacity: '0', ease: Sine.easeInOut,});
        tl.to(firstText, {delay: 4, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
        tl.to(firstText, 0, {
            display: "none", onComplete: function () {
                firstText.remove();
            }
        });
        // second screen
        tl.to(secondText, 0, {display: "block"});
        tl.from(secondText, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut,});
        tl.from(exploreBtn, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut});


        //third screen and main screen
        exploreBtn.on('click', function () {
            tl.to(secondText, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(exploreBtn, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut}, "<");
            tl.to(secondText, 0, {
                display: "none", onComplete: function () {
                    secondText.remove();
                }
            });
            tl.to(exploreBtn, 0, {
                display: "none", onComplete: function () {
                    exploreBtn.remove();
                }
            });
            // third screen
            tl.to(thirdText, 0, {display: "block"});
            tl.from(thirdText, {delay: 0, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(thirdText, {delay: 1.5, duration: 1.3, opacity: '0', ease: Sine.easeInOut});
            tl.to(thirdText, 0, {
                display: "none", onComplete: function () {
                    thirdText.remove();
                }
            }, ">-0.2");
            // fourth and main screen
            tl.to(fourthTitle, {display: "block"});
            tl.from(fourthTitle, {duration: 1.3, opacity: '0', ease: Sine.easeInOut}, ">-0.3");
            tl.to(trendsList, {display: "flex",onComplete: function () {
                    gsap.to($('.logo'), {duration: 1.5, top: '6vh', ease: Sine.easeInOut});
                }}, "<");
            tl.from(trendsList, {duration: 1, opacity: "0"}, "<+0.3");
            tl.from(trendsImg, {duration: 2.5, width: "4px"}, "<");
            tl.from(trendsItemText, {duration: 1, opacity: "0"}, "<");
            tl.to(fourthTitle, {delay: 1.5, duration: 1.3, opacity: "0"}, "<");
            tl.to(preloaderText, 0, {
                display: "none", onComplete: function () {
                    preloaderText.remove();
                }
            });
            tl.to(mainTitle, 0, {
                display: "block", onComplete: function () {
                    gsap.from(mainTitle, {duration: 1.3, opacity: '0', ease: Sine.easeInOut});
                    gsap.to(screenSwitchBtn, {duration: 1.3, opacity: '0.8', ease: Sine.easeInOut});
                    gsap.to(iconsBlock, {duration: 1.3, opacity: '1', ease: Sine.easeInOut});
                }
            }, "<-1");

            // ------------ Wrap our title words in tag--------------

            let OurString = $('#trend-title-text').html(),
                NewOurString = "";

            OurString = OurString.split(" ");
            for (let i = 0; i < OurString.length; i++) {
                NewOurString = NewOurString + "<div class='word-overflow'><span>" + OurString[i] + "</span></div>";
            }
            $('#trend-title-text').html(NewOurString);
            $('#trend-title-descr').html('description').css('opacity', '0');

            // -------------trend item hover--------
            tl.to($(".trends-item"), 1, {
                onComplete: function () {
                    $(".trends-item").addClass('trends-item-hover');

                    //--------------Mouseover--------------
                    $('.trends-item').mouseenter(function () {
                        $('.trends-item').css('opacity', '0.3');
                        $(this).css('opacity', '1');
                        $('#trend-title-descr').css('opacity', '1');

                        //-------------Wrap trend title words in tag------
                        let trendTitle = $(this).attr('data-title'),
                            newTrendTitle = " ";
                        trendTitle = trendTitle.split(" ");
                        for (let i = 0; i < trendTitle.length; i++) {
                            newTrendTitle = newTrendTitle + "<div class='word-overflow'><span>" + trendTitle[i] + "</span></div>";
                        }
                        $('#trend-title-text').html(newTrendTitle);

                        //-------------Wrap trend descr words in tag------

                        // let trendDescr = $(this).attr('data-descr'),
                        //     newTrendDescr = " ";
                        // trendDescr = trendDescr.split(" ");
                        // for (let i = 0; i < trendDescr.length; i++) {
                        //     newTrendDescr = newTrendDescr + "<div class='word-overflow'><span>" + trendDescr[i] + "</span></div>";
                        // }
                        // $('#trend-title-descr').html(newTrendDescr);
                        //
                        TweenMax.staggerFrom($('#trend-title-text span'), 0.3, {y: 100, stagger: 0.03});
                        // TweenMax.staggerFrom($('#trend-title-descr span'), 0.3, {y: 100, stagger: 0.03});

                    });

                    //---------------------Mouseout----------
                    $('.trends-item').mouseleave(function () {
                        $('.trends-item').css('opacity', '1');
                        $('#trend-title-text').html(NewOurString);
                        $('#trend-title-descr').css('opacity', '0');
                        TweenMax.staggerFrom($('#trend-title-text span'), 0.3, {y: 100, stagger: 0.03});
                    });

                    // ----------------------hover on last trend item---------------
                    $('.trends-item-agencies').mouseenter(function () {
                        // $('.trends-item-agencies .text').hide().fadeIn(800);
                        $('.trends-item-agencies .text').hide();
                        // $('.trends-item-agencies img').css('opacity', '1');
                        $('.trends-item-agencies .text-hover').addClass('active');

                    });

                    $('.trends-item-agencies').mouseleave(function () {
                        // $('.trends-item-agencies .text').hide().fadeIn(800);
                        // $('.trends-item-agencies img').css('opacity', '0');
                        $('.trends-item-agencies .text').fadeIn(800);
                        $('.trends-item-agencies .text-hover').removeClass('active');
                    });
                }
            });
        });
    }


});







