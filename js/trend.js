$(document).ready(function () {
    if ($(window).width() > 992) {
        $('video').removeAttr('controls');
        // Loader logo
        $('.load-percent span').countTo({
            from: 0,
            to: 100,
            speed: 2000,
            refreshInterval: 10,
            onUpdate: function (value) {
                let percent = $('.load-percent span').html() + '%';
                $('.logo-white-wrap').css('width', percent);
            },
        });

        // Preloader
        let tl = gsap.timeline({repeat: 0, repeatDelay: 0});


        tl.to($('.trend-preloader'), {delay: 3, duration: 1, opacity: '0'});
        tl.to($('.trend-preloader'), {display: "none"});
        tl.to($('.trend-content'), {display: "block"}, '<');
        tl.from($('.trend-content'), {duration: 1, opacity: '0'});

        function animateImg(item) {
            gsap.from(item, {
                opacity: 0,
                duration: 1.5,
                rotate: '-5deg',
                scale: '.8',
                ease: Sine.easeInOut,
            });
        }

        gsap.to($('.trend-preloader'), {delay: .1, duration: .3, opacity: "1"});
        gsap.from($('.preloader-title'), {duration: 1, opacity: "0"});

        animateImg('.main-image');
        animateImg('.bottom-img');
        animateImg('.left-img');
        animateImg('.right-img');

        // Color pallete

        $('.color-pallete').on('click', function () {
            $(this).toggleClass('active');
        });
        $('.color-pallete').on('mouseleave', function () {
            $(this).removeClass('active');
        });

        $('.pallete-item').on('mouseover', function () {
            $(this).find('.color-code').show();
        });

        $('.pallete-item').on('mouseleave', function () {
            $(this).find('.color-code').hide();
        });

        // Copy color on click pallete

        $('.pallete-item').on('click', function () {
            let code = $(this).find('.color-code'),
                $tmp = $("<textarea>");

            $("body").append($tmp);
            $tmp.val($(code).text()).select();
            document.execCommand("copy");
            $tmp.remove();
        });


        // trend left title hover
        $('.trend-title-block').on('mouseover', function () {
            $(this).addClass('bg-trend');
            // $(this).addClass('hover');
            // $('.trend-title-block .title').hide();
            // $('.trend-title-block .title-hover').fadeIn();
        });

        $('.trend-title-block').on('mouseout', function () {
            $(this).removeClass('bg-trend');
            // $('.trend-title-block .title').fadeIn();
            // $('.trend-title-block .title-hover').hide();
        });

        $('.trend-slider').on('init', function (event, slick) {
            fileInfo();
        });

        function fileInfo() {
            let name = $('.slick-active').attr('data-author'),
                number = $('.slick-active').attr('data-number');
            $('#author-name').html(name);
            $('#file-number').html(number);
        }

        $('.trend-slider').on('init', function (event, slick) {
            fileInfo();
        });
        

        $('.trend-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            fileInfo();
        });
    }


    // slider

    $('.trend-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'ease-in-out',
        arrows: true,
        prevArrow: "<button type='button' class='slick-btn slick-prev'></button>",
        nextArrow: "<button type='button' class='slick-btn slick-next'></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows: false,
                    fade: false
                }
            }]
    });


    $('.trend-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {

        $(".slider-video").each(function (index, element) {
            $('.slider-video').get(index).pause();
        });

        if ($(".slider-item-video").hasClass("slick-active")) {
            $('.slick-active').find('.slider-video').get(0).play();
        } else {
            $('.slider-item-video').find('.slider-video').get(0).pause();
        }

        if (currentSlide === 0) {
            $('.color-pallete').show();
        } else {
            $('.color-pallete').hide();
        }
    });


    // Info block and  Tabs info
    $('.trend-title-block').on('click', function () {
        $('#info-block').addClass('visible');
        $('.tab-info-overlay').show()
        $('.color-pallete').hide();

        if ($(window).width() < 992) {
            $('.trend-slider .slick-slide').css('height', '50vh');
        }
    });
    $('.close-info-btn, .tab-info-overlay').on('click', function () {
        $('.color-pallete').css('display', 'flex');
        $('#info-block').removeClass('visible');
        $('.tab-info-overlay').hide()

        if ($(window).width() < 992) {
            $('.trend-slider .slick-slide').css('height', '100vh');
        }
    });

    $('.tabs-nav').on('click', '.nav-item:not(.bg-trend-tab-active)', function () {
        $(this)
            .addClass('bg-trend-tab-active').siblings().removeClass('bg-trend-tab-active')
            .closest('.info-tabs').find('.tabs-content-item').removeClass('active').eq($(this).index()).addClass('active');
    });


});