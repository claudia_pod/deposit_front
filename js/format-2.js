// ----------------------Second format------------------
$(document).ready(function () {

    // ------------ Wrap our title words in title and descr--------------

    function wordWrap(item) {
        $(item).each(function (index) {
            let OurString = $(this).html(),
                NewOurString = "";

            OurString = OurString.split(" ");
            for (let i = 0; i < OurString.length; i++) {
                NewOurString = NewOurString + "<div class='word-overflow'><span class='animate-word'>" + OurString[i] + "</span></div>";
            }
            $(this).html(NewOurString);
        });
    };

    wordWrap('.trend-item-title');
    // wordWrap('.trend-item-descr');


    //-----------Logo loading function----------------
    function logoLoader() {
        $('.load-percent span').countTo({
            from: 0,
            to: 100,
            speed: 1700,
            refreshInterval: 10,
            onUpdate: function (value) {
                let percent = $('.load-percent span').html() + '%';
                $('.logo-white-wrap').css('width', percent);
            },
        });
    }

    //-----------Animate image function----------------
    function animateImage() {
        gsap.from($('.trend-image-1'), {
            opacity: 0,
            duration: 1.5,
            rotate: '-5deg',
            scale: '.8',
            ease: Sine.easeInOut,
        });
        gsap.from($('.trend-image-2'), {
            opacity: 0,
            duration: 1.5,
            rotate: '-6deg',
            scale: '.8',
            ease: Sine.easeInOut,
        });
        gsap.from($('.trend-image-3'), {
            opacity: 0,
            duration: 1.5,
            rotate: '20deg',
            scale: '.8',
            ease: Sine.easeInOut,
        });
    }

    $('.link-to-trend').on('click', function (event){
        event.preventDefault();
        $('.preloader-logo').show();
        $('.logo-format-2').hide();

        logoLoader();

        let url = $(this).attr('href');
        setInterval(function() {
            window.location = url;
        }, 2000);
    });

    //------------Load first screen------------
    gsap.to($('.second-format'), {delay: .1, opacity: 1, duration: 1});
    // logoLoader();
    animateImage();

    //-------------Bg color body the same first trend-------------
    let bgColorStart = $('.trend-screen:first-child').attr('data-color');
    $('body').css('background-color', bgColorStart);

    //-----Animate title and descr on load page-------------
    TweenMax.staggerFrom($('.animate-word'), 1, {y: 100, stagger: 0.03});
    // TweenMax.staggerFrom($('.trend-item-title .animate-word'), 1, {y: 100, stagger: 0.03});

    //-------One page scroll libs----------
    $(".second-format").onepage_scroll({
        sectionContainer: "section",
        easing: "step-end",
        animationTime: 700,
        pagination: false,
        updateURL: false,
        loop: true,
        keyboard: true,
        direction: "vertical",
        beforeMove: function (index) {
            gsap.to($('.trend-images-screens'), {opacity: 0, duration: .5});
            gsap.to($('.trend-item-text'), {opacity: 0, duration: .5});
            // $('.trend-images-screens').fadeOut();
            // $('.trend-item-text').fadeOut();
            // logoLoader();
            let bgColor = $('.trend-screen.active').attr('data-color');
            $('body').css('background-color', bgColor);

        },
        afterMove: function (index) {
            gsap.to($('.trend-images-screens'), {opacity: 1, duration: .5});
            gsap.to($('.trend-item-text'), {opacity: 1, duration: .5});
            // $('.trend-images-screens').fadeIn();
            // $('.trend-item-text').fadeIn();
            animateImage();
            TweenMax.staggerFrom($('.trend-screen.active .animate-word'), 0.5, {y: 100, stagger: 0.03});
        },
    });

    //-----------Scroll down btn function on click----------------
    $(".scroll-down-btn").on('click', function () {
        var section_index = $('.second-format section.active').index();
        var section_length = $('.second-format section').length - 1;
        if (section_index == section_length) {
            $(".second-format").moveTo(0);
        }
        $('.second-format').moveDown();
    });

    $('.agencies-block').on('mouseover', function () {
        $('.trend-screen.active .trend-images-screens, .trend-screen.active .trend-item-text').css('opacity', '.5');
    });


    $('.agencies-block').on('mouseout', function () {
        $('.trend-screen.active .trend-images-screens, .trend-screen.active .trend-item-text').css('opacity', '1');
    });
});


